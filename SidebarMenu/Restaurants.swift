//
//  Restaurants.swift
//  SidebarMenu
//
//  Created by Hjalti Steinar on 2/11/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import Foundation

class Restaurants {
    
    // MARK : Properties
    
    let storeArray: [String] = ["Hagkaup", "Nettó", "10-11", "Bónus"]
    let storeDict: [String: String] = ["Hagkaup": "hagkaup", "Nettó": "netto", "10-11": "tiuEllefu", "Bónus": "bonus"]
    let openFromDict: [String: Int] = ["Hagkaup": 2500, "Nettó": 1000, "10-11": 2500, "Bónus": 1000]
    let openToDict: [String: Int] = ["Hagkaup": 2500, "Nettó": 1800, "10-11": 2500, "Bónus": 1830]
    
    var openFromToDict = [String: String]()
    
    
    // MARK : Func
    
    func runTime() {
        
        let simple = SimpleYourLife()
        
        openFromToDict = simple.runTime(openFromDict, openToDict: openToDict)
        
    }
    
    init() {
        
        runTime()
        
    }
    
}