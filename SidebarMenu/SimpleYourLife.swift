//
//  SimpleYourLife.swift
//  SidebarMenu
//
//  Created by Hjalti Steinar on 2/11/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import Foundation

public class SimpleYourLife {
    
    func runTime(openFromDict: [String: Int], openToDict: [String: Int]) -> [String: String] {
        
        var openFromToDict = [String: String]()
        
        var openFromStringDict = [String: String]()
        var openToStringDict = [String: String]()
        
        for (name, time) in openFromDict {
    
            var intToString = Array(String(time).characters)
            
            if time < 1000 {
                intToString.insert(":", atIndex: 1)
            } else {
                intToString.insert(":", atIndex: 2)
            }
            
            if time >= 2500 {
                openFromStringDict[name] = "Opið 24/7"
            } else {
                openFromStringDict[name] = String(intToString)
            }
        }
    
        for (name, time) in openToDict {
    
            var intToString = Array(String(time).characters)
    
            if time < 1000 {
                intToString.insert(":", atIndex: 1)
            } else {
                intToString.insert(":", atIndex: 2)
            }
            
            if time >= 2500 {
                openToStringDict[name] = "Opið 24/7"
            } else {
                openToStringDict[name] = String(intToString)
            }
        }
    
        for (name, time) in openFromStringDict {
            if time == "Opið 24/7" {
                openFromToDict[name] = openFromStringDict[name]
            } else {
                openFromToDict[name] = openFromStringDict[name]! + "-" + openToStringDict[name]!
            }
        }
        
        return openFromToDict
    }
}