//
//  VeitingaTableViewController.swift
//  SidebarMenu
//
//  Created by Hjalti Steinar on 2/10/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit

class VeitingaTableViewController: UITableViewController {
    @IBOutlet weak var menuButton:UIBarButtonItem!
    
    let stores = Restaurants()
    let time = Time()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 3
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! VeitingaTableViewCell
        
        // Configure the cell...
        
        cell.storeName.text = stores.storeArray[indexPath.row]
        cell.storeImage.image = UIImage(named: stores.storeDict[stores.storeArray[indexPath.row]]!)
        cell.whenOpen.text = stores.openFromToDict[stores.storeArray[indexPath.row]]
        cell.openImage.image = time.compareTime(stores.openFromDict[stores.storeArray[indexPath.row]]!)
        
        return cell
    }
}
