//
//  FastfoodStores.swift
//  SidebarMenu
//
//  Created by Hjalti Steinar on 2/11/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import Foundation

class FastfoodStores {
    
    // MARK : Properties
    
    let storeArray: [String] = ["Hlölli", "Subway", "Serrano"]
    let storeDict: [String: String] = ["Hlölli": "hlolli", "Subway": "subway", "Serrano": "serrano"]
    let openFromDict: [String: Int] = ["Hlölli": 0800, "Subway": 2500, "Serrano": 1000]
    let openToDict: [String: Int] = ["Hlölli": 2300, "Subway": 2500, "Serrano": 2300]
    
    var openFromToDict = [String: String]()
    
    
    // MARK : Func
    
    func runTime() {
        
        let simple = SimpleYourLife()
        
        openFromToDict = simple.runTime(openFromDict, openToDict: openToDict)
        
    }
    
    init() {
        
        runTime()
        
    }
    
}