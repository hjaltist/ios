//
//  NewsTableViewCell.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit

class MatvoruTableViewCell: UITableViewCell {
    
    @IBOutlet weak var storeImage:UIImageView!
    @IBOutlet weak var openImage:UIImageView!
    @IBOutlet weak var storeName:UILabel!
    @IBOutlet weak var whenOpen: UILabel!
    @IBOutlet weak var whereOpen: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
