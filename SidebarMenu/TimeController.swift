//
//  TimeController.swift
//  SidebarMenu
//
//  Created by Hjalti Steinar on 2/14/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import Foundation
import UIKit

class Time : NSDate {
    
    let onImage = UIImage(named: "oncircle")
    let offImage = UIImage(named: "offcircle")
    
    func hour() -> Int
    {
        //Get Hour
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.Hour, fromDate: NSDate())
        let hour = components.hour
        
        //Return Hour
        return hour
    }
    
    
    func minute() -> Int
    {
        //Get Minute
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.Minute, fromDate: NSDate())
        let minute = components.minute
        
        //Return Minute
        return minute
    }
    
    func compareTime(timeEntry: Int) -> UIImage {
        print(timeEntry)
        if timeEntry == 2500 {
            print("Return 2500")
            return onImage!
        }
        
        let hours = hour() * 100
        print(hours)
        if hours >= timeEntry {
            return offImage!
        } else {
            return onImage!
        }
    }
    
}
